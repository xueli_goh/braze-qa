package com.appboy.cordova;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.appboy.AppboyFirebaseMessagingService;
import android.util.Log;
import android.support.v4.content.LocalBroadcastManager;
import android.content.Intent;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
	
	private static final String TAG = "FirebaseMessagingService";
	private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
		Log.d(TAG, "declaring broadcaster");
        broadcaster = LocalBroadcastManager.getInstance(this);
		Log.d(TAG, "declared broadcaster");
    }
	
	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		AppboyPlugin plugin = new AppboyPlugin();
		
		Log.d(TAG, "From: " + remoteMessage.getFrom());
		super.onMessageReceived(remoteMessage);
		Log.d(TAG, "Message data payload: " + remoteMessage.getData());
		
		if (AppboyFirebaseMessagingService.handleBrazeRemoteMessage(this, remoteMessage)) {
			// This Remote Message originated from Braze and a push notification was displayed.
			// No further action is needed.
			Log.d(TAG, "This Remote Message originated from Braze.");
		} else {
			// This Remote Message did not originate from Braze.
			// No action was taken and you can safely pass this Remote Message to other handlers.
			Log.d(TAG, "This Remote Message did not originate from Braze.");
		}
		Log.d(TAG, "Received push notification: " + remoteMessage);
		Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
		
		Intent intent = new Intent("onMessageReceived");
		intent.putExtra("event", "onMessageReceived");
		intent.putExtra("data", remoteMessage.getNotification().getBody());
		Log.d(TAG, "Intent: " + intent);
        broadcaster.sendBroadcast(intent);
		Log.d(TAG, "sent broadcast");
	}
}